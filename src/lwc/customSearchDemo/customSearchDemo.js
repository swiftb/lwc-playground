/**
 * Created by bswif on 3/28/2021.
 */


import { LightningElement, track, wire } from 'lwc';
import queryAllCons from '@salesforce/apex/queryAllCons.queryAllCons';

export default class CustomSearchDemo extends LightningElement {
    @track currentRec = '';
    @track theRecs;

    @wire(queryAllCons)
    wiredCons({error,data}){
        if(error){
            console.log('err: '+error.message);
        }else if(data){
            this.theRecs = data;
        }
    }
/*
    @track theRecs = [
        {
            label:'Michael',
            value:"Michael Scott"
        },
        {
            label:'Jim',
            value:"Jim Halpert"
        },
        {
            label:'Pam',
            value:"Pam Beesly"
        },
        {
            label:'Kevin',
            value:"Kevin Malone"
        },
        {
            label:'Stanley',
            value:"Stanley Hudson"
        },
        {
            label:'Andy',
            value:"Andy Bernard"
        },
        {
            label:'Darryl',
            value:"Darryl Philbin"
        },
        {
            label:'Angela',
            value:"Angela Martin"
        },
        {
            label:'Meredith',
            value:"Meredith Palmer"
        },
        {
            label:'Toby',
            value:"Toby Flenderson"
        }
    ];
*/
    handleSelect(event){
        if(event.detail.record){
            this.currentRec = event.detail.record.value;
        }else{
            this.currentRec = undefined;
        }
    }
}