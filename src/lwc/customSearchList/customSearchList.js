/**
 * Created by bswif on 3/28/2021.
 */

import { LightningElement, api } from 'lwc';

export default class CustomSearchList extends LightningElement {
    @api record;

    handleSelect(event){
        event.preventDefault();
        const selectedRecord = new CustomEvent(
            "select",
            {
                detail : this.record.value
            }
        );
        this.dispatchEvent(selectedRecord);

    }
}