/**
 * Created by bswif on 3/28/2021.
 */

import { LightningElement, track, api } from 'lwc';
export default class CustomSearch extends LightningElement {
    @api allRecords;
    @track currentRecords;
    @track error;
    @track selectedRecord;

    handleOnchange(event){
        this.currentRecords = [];
        let searchKey = event.detail.value;
        if(searchKey){
            for(let theRec of this.allRecords){
                if(theRec.label.toLowerCase().startsWith(searchKey.toLowerCase())){
                    this.currentRecords.push(theRec);
                }
            }

            for(let theRec of this.allRecords){
                if(!(theRec.label.toLowerCase().startsWith(searchKey)) && theRec.label.toLowerCase().includes(searchKey.toLowerCase())){
                    this.currentRecords.push(theRec);
                }
            }
        }
    }
    handleSelect(event){
        const selectedRecordId = event.detail;
        this.selectedRecord = this.allRecords.find( record => record.value === selectedRecordId);
        const selectedRecordEvent = new CustomEvent(
            "selectedrec",
            {
                detail : { record : this.selectedRecord}
            }
        );
        this.dispatchEvent(selectedRecordEvent);
    }

    handleRemove(event){
        event.preventDefault();
        this.selectedRecord = undefined;
        this.currentRecords = undefined;
        this.error = undefined;
        const selectedRecordEvent = new CustomEvent(
            "selectedrec",
            {
                detail : { record : this.selectedRecord}
            }
        );
        this.dispatchEvent(selectedRecordEvent);
    }


}