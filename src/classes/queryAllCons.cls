/**
 * Created by bswif on 3/29/2021.
 */

public with sharing class queryAllCons {
    @AuraEnabled(Cacheable = true)
    public static List<ConWrapper> queryAllCons(){
        List<ConWrapper> theCons = new List<ConWrapper>();
        for(Contact theCon : [SELECT Id, FirstName, LastName FROM Contact]){
            theCons.add(new ConWrapper(theCon.FirstName+' '+theCon.LastName,theCon.Id));
        }
        return theCons;
    }


    public class ConWrapper{
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;

        public ConWrapper(String labelParam, String valueParam){
            label = labelParam;
            value = valueParam;
        }
    }
}